#!/bin/bash

function show_use {
cat <<Show-Usage
        
Usage: $0 alias_key password_key
         
Adds a new SSL Key to your keystore as defined in the script.
         
This script uses values in your ~/.gradle/gradle.properties file.
In order to use this script you must modify it as explained in the script.
         
Your gradle.properties file must have a key named KEYSTORE which provides the name and location of your keystore.
Your gradle.properties file must have a key name KEYSTORE_PASSWORD which provides the password to your keystore.
        
The script will ask you to enter the name for the new key as well as a password. It will not ask you to repeat
your password as you are probably copy pasting it from a password safe anyway. (which you should do)
         
Once the key is created the script will backup your key store and will add the new key values to your gradle.properties file.
It will also print out the new entries from your gradle.properties so that you can use them in your gradle.build script.
        
Show-Usage
}


# User information. Edit this to show your correct information. This shouldn't change often.
COUNTRY='Canada'
STATE='Quebec'
LOCALITY='Laval'
ORGANISATION='Rise Software'
ORGANISATIONAL_UNIT='Mobile Development'
FIRST_LASTNAME='Mike Wallace'
EMAIL='mike.wallace@risesoftware.com'

#
# That's it, nothing else to configure.
#

# Key store information this should not change. The key store will be created if it does not exist
KEYSTORE=$(awk -F "=" '/KEYSTORE_FILE/ {print $2}' ~/.gradle/gradle.properties)
KEYSTORE_PASSWORD=$(awk -F "=" '/KEYSTORE_PASSWORD/ {print $2}' ~/.gradle/gradle.properties)


# Generate the dname 
DNAME="CN=$FIRST_LASTNAME/$EMAIL, OU=$ORGANISATIONAL_UNIT, O=$ORGANISATION, L=$LOCALITY, S=$STATE, C=$COUNTRY"

# Get the name of the key and the password
echo "Please, enter the name for the new key: "
read ALIAS_NAME

echo "Please enter the password for the new key: "
read KEY_PASSWORD


if [ "$ALIAS_NAME" == "" ]; then
   show_use;
   exit;
fi

if [ "$KEY_PASSWORD" == "" ]; then
   show_use;
   exit;
fi

# if the keystore exists, then we make it writeable
if [ -e $KEYSTORE ]; then
   chmod u+w ${KEYSTORE};
fi


# Generate the key
 keytool -genkey -v -alias "$ALIAS_NAME" -keypass "$KEY_PASSWORD" -keystore "$KEYSTORE" -storepass $KEYSTORE_PASSWORD -dname "$DNAME"  -keyalg RSA -keysize 2048 -validity 10000


# make sure that the keytool worked!!!! 
retVal=$?; 
if [[ $retVal != 0 ]]; then
   echo Error trying to generate the key pair
   exit $retVal; 
fi

# Make the keystore read-only
chmod uog-w $KEYSTORE


# Make a backup of the keystore with the new key in it. Then make the copy read-only too!
if [ -e $KEYSTORE.SAFE ]; then
   chmod u+w $KEYSTORE.SAFE;
fi
cp $KEYSTORE $KEYSTORE.SAFE
chmod uog-w $KEYSTORE.SAFE


#Add the key and password to gradle.properties
cat >> ~/.gradle/gradle.properties <<Props

${ALIAS_NAME}_ALIAS=${ALIAS_NAME}
${ALIAS_NAME}_PASSWORD=${KEY_PASSWORD}
Props

# Give some helpful information to the user, because we're so nice.
cat <<End-of-message

New keys for you gradle.build:

    signingConfigs {
        release {
            storeFile file(KEYSTORE_FILE)
            storePassword KEYSTORE_PASSWORD
            keyAlias ${ALIAS_NAME}_ALIAS
            keyPassword ${ALIAS_NAME}_PASSWORD
        }
    }

End-of-message
